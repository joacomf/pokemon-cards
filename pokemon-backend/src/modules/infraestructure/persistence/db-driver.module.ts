import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { PokemonEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon.entity';
import { PokemonTypeEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon-type.entity';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        dialect: 'postgres',
        host: configService.get<string>('DB_HOST'),
        port: parseInt(configService.get<string>('DB_PORT')),
        username: configService.get<string>('DB_USERNAME'),
        password: configService.get<string>('DB_PASSWORD'),
        database: configService.get<string>('DB_DATABASE'),
        models: [PokemonEntity, PokemonTypeEntity],
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DbDriverModule {}
