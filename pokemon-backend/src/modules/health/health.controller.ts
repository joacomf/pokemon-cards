import { Controller, Get } from '@nestjs/common';

@Controller()
export class HealthController {
  constructor() {}

  @Get('/health')
  getHealth(): { status: string } {
    return { status: 'OK' };
  }
}
