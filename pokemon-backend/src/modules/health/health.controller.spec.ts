import { Test, TestingModule } from '@nestjs/testing';
import { HealthController } from './health.controller';
import * as request from 'supertest';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';

describe('HealthController', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HealthController],
    }).compile();

    app = module.createNestApplication<NestApplication>();
    await app.init();
  });

  describe('[GET] /health', () => {
    it('Should get application health check', async () => {
      const response = await request(app.getHttpServer())
        .get('/health')
        .expect(HttpStatus.OK);

      expect(response.body).toEqual({ status: 'OK' });
    });
  });
});
