import { Module } from '@nestjs/common';
import { HealthModule } from './health/health.module';
import { PokemonModule } from '@/modules/pokemon/pokemon.module';
import { DbDriverModule } from '@/modules/infraestructure/persistence/db-driver.module';
import { ConfigModule } from '@nestjs/config';
import config from '@/modules/infraestructure/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config],
    }),
    DbDriverModule,
    HealthModule,
    PokemonModule,
  ],
  controllers: [],
  providers: [],
})
export class Modules {}
