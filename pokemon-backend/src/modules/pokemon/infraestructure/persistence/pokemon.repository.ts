import { Injectable } from '@nestjs/common';
import { Pokemon, PokemonBuilder } from '@/modules/pokemon/domain/pokemon';
import { InjectModel } from '@nestjs/sequelize';
import { PokemonEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon.entity';
import { PokemonType } from '@/modules/pokemon/domain/pokemonType';
import { PokemonTypeEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon-type.entity';
import { CreatePokemonDto } from '@/modules/pokemon/infraestructure/dto/create-pokemon.dto';

@Injectable()
export class PokemonRepository {
  constructor(
    @InjectModel(PokemonEntity)
    private pokemonModel: typeof PokemonEntity,
  ) {}

  async create(createPokemonDto: CreatePokemonDto): Promise<PokemonEntity> {
    return await this.pokemonModel.create(createPokemonDto);
  }

  async getAll(): Promise<Pokemon[]> {
    const pokemons = await this.pokemonModel.findAll({
      include: [PokemonTypeEntity],
    });

    return pokemons.map((pokemon) =>
      PokemonBuilder.create(pokemon.name)
        .withId(pokemon.id)
        .withImage(pokemon.image)
        .withType(pokemon.type.name as PokemonType)
        .withRarity(pokemon.rarity)
        .withExpansion(pokemon.expansion)
        .build(),
    );
  }

  async deleteAll() {
    await this.pokemonModel.truncate({ where: {} });
  }
}
