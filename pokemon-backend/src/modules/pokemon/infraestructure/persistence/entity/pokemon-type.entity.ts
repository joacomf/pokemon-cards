import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { PokemonEntity } from './pokemon.entity';

@Table({ modelName: 'pokemon_types', timestamps: false })
export class PokemonTypeEntity extends Model<PokemonTypeEntity> {
  @Column({ allowNull: false, unique: true })
  name: string;

  @HasMany(() => PokemonEntity)
  pokemons: PokemonEntity[];
}
