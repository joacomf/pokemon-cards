import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Rarity } from '@/modules/pokemon/domain/rarity';
import { PokemonTypeEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon-type.entity';

@Table({ modelName: 'pokemons' })
export class PokemonEntity extends Model<PokemonEntity> {
  @Column({ allowNull: false })
  name: string;

  @Column({ allowNull: false })
  image: string;

  @Column({
    allowNull: false,
    type: 'ENUM',
    values: ['COMMON', 'UNCOMMON', 'RARE'],
  })
  rarity: Rarity;

  @ForeignKey(() => PokemonTypeEntity)
  @Column({ allowNull: false })
  typeId: number;

  @BelongsTo(() => PokemonTypeEntity)
  type: PokemonTypeEntity;

  @Column({ allowNull: false })
  expansion: string;
}
