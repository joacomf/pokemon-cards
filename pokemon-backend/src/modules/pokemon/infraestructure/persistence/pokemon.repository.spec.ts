import { Test, TestingModule } from '@nestjs/testing';
import { SequelizeModule } from '@nestjs/sequelize';
import { PokemonRepository } from './pokemon.repository';
import { PokemonEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon.entity';
import { CreatePokemonDto } from '@/modules/pokemon/infraestructure/dto/create-pokemon.dto';
import { Pikachu } from '@test/fixtures/pokemons.fixture';
import { PokemonTypeEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon-type.entity';
import { migrateDBForTests } from '@test/config/db';

describe('PokemonRepository', () => {
  let moduleRef: TestingModule;
  let pokemonRepository: PokemonRepository;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRoot({
          dialect: 'sqlite',
          storage: 'db/.cache/data.sqlite3',
          models: [PokemonEntity, PokemonTypeEntity],
        }),
        SequelizeModule.forFeature([PokemonEntity, PokemonTypeEntity]),
      ],
      providers: [PokemonRepository],
    }).compile();

    migrateDBForTests();

    pokemonRepository = moduleRef.get(PokemonRepository);
  });

  beforeEach(async () => {
    await pokemonRepository.deleteAll();
  });

  afterAll(async () => {
    await moduleRef.close();
  });

  describe('findAll', () => {
    it('should find all pokemons', async () => {
      await givenExistsPokemonsInStorage();

      const found = await pokemonRepository.getAll();

      expect(found).toEqual([
        {
          id: 1,
          name: 'Pikachu',
          image:
            'https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png',
          rarity: 'COMMON',
          type: 'Electric',
          expansion: 'Base Set',
        },
      ]);
    });

    it('should return empty array if there are no pokemons', async () => {
      const found = await pokemonRepository.getAll();

      expect(found).toEqual([]);
    });
  });

  async function givenExistsPokemonsInStorage() {
    const createPikachu = new CreatePokemonDto(
      Pikachu.name,
      Pikachu.image,
      Pikachu.rarity,
      4,
      Pikachu.expansion,
    );
    await pokemonRepository.create(createPikachu);
  }
});
