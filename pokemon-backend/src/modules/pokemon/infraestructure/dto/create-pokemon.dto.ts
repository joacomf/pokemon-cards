import { IsNotEmpty, IsString, IsEnum, IsNumber } from 'class-validator';
import { Rarity } from '@/modules/pokemon/domain/rarity';

export class CreatePokemonDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  image: string;

  @IsNotEmpty()
  @IsEnum(Rarity)
  rarity: Rarity;

  @IsNotEmpty()
  @IsNumber()
  typeId: number;

  @IsNotEmpty()
  @IsString()
  expansion: string;

  constructor(
    name: string,
    image: string,
    rarity: Rarity,
    typeId: number,
    expansion: string,
  ) {
    this.name = name;
    this.image = image;
    this.rarity = rarity;
    this.typeId = typeId;
    this.expansion = expansion;
  }
}
