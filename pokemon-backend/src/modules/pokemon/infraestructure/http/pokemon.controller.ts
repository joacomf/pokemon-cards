import { Controller, Get } from '@nestjs/common';
import { PokemonService } from '@/modules/pokemon/app/pokemon.service';

@Controller()
export class PokemonController {
  constructor(private service: PokemonService) {}

  @Get('pokemons')
  getAll() {
    return this.service.getAll();
  }
}
