import { Test, TestingModule } from '@nestjs/testing';
import { NestApplication } from '@nestjs/core';
import * as request from 'supertest';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { Gengar, Jigglypuff } from '@test/fixtures/pokemons.fixture';
import { PokemonController } from '@/modules/pokemon/infraestructure/http/pokemon.controller';
import { PokemonService } from '@/modules/pokemon/app/pokemon.service';
import { instance, mock, when } from 'ts-mockito';

describe('[Controller] PokemonController', () => {
  let app: INestApplication;
  let serviceMock: PokemonService;

  const POKEMONS = [Jigglypuff, Gengar];

  beforeEach(async () => {
    serviceMock = mock(PokemonService);
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PokemonController],
      providers: [{ provide: PokemonService, useValue: instance(serviceMock) }],
    }).compile();

    app = module.createNestApplication<NestApplication>();
    await app.init();
  });

  describe('[GET] /pokemons', () => {
    it('should get all pokemons', async () => {
      when(serviceMock.getAll()).thenResolve(POKEMONS);

      const response = await request(app.getHttpServer())
        .get('/pokemons')
        .expect(HttpStatus.OK);

      expect(response.body).toEqual(POKEMONS);
    });
  });
});
