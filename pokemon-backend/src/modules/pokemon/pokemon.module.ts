import { Module } from '@nestjs/common';
import { PokemonRepository } from '@/modules/pokemon/infraestructure/persistence/pokemon.repository';
import { PokemonController } from '@/modules/pokemon/infraestructure/http/pokemon.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { PokemonTypeEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon-type.entity';
import { PokemonEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon.entity';
import { PokemonService } from '@/modules/pokemon/app/pokemon.service';

@Module({
  imports: [SequelizeModule.forFeature([PokemonEntity, PokemonTypeEntity])],
  controllers: [PokemonController],
  providers: [PokemonService, PokemonRepository],
})
export class PokemonModule {}
