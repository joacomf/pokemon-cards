import { Injectable } from '@nestjs/common';
import { Pokemon } from '@/modules/pokemon/domain/pokemon';
import { PokemonRepository } from '@/modules/pokemon/infraestructure/persistence/pokemon.repository';

@Injectable()
export class PokemonService {
  constructor(private pokemonRepository: PokemonRepository) {}

  async getAll(): Promise<Pokemon[]> {
    return this.pokemonRepository.getAll();
  }
}
