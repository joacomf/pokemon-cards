import { PokemonService } from '@/modules/pokemon/app/pokemon.service';
import { Gyarados, Mewtwo } from '@test/fixtures/pokemons.fixture';
import { PokemonRepository } from '@/modules/pokemon/infraestructure/persistence/pokemon.repository';
import { instance, mock, when } from 'ts-mockito';

describe('Pokemon Service', () => {
  let pokemonService: PokemonService;
  let repository: PokemonRepository;
  const POKEMON_LIST = [Gyarados, Mewtwo];

  beforeEach(() => {
    repository = mock(repository);
    pokemonService = new PokemonService(instance(repository));
  });

  it('should return a list of pokemons', async () => {
    when(repository.getAll()).thenResolve(POKEMON_LIST);
    const pokemons = await pokemonService.getAll();
    expect(pokemons).toEqual(POKEMON_LIST);
  });
});
