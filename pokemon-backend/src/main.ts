import { NestFactory } from '@nestjs/core';
import { Modules } from './modules/modules';

async function bootstrap() {
  const app = await NestFactory.create(Modules);
  app.enableCors();
  await app.listen(process.env.PORT || 3001);
}
bootstrap();
