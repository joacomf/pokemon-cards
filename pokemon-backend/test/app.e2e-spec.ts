import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { Modules } from '@/modules/modules';
import { migrateDBForTests } from '@test/config/db';
import { SequelizeModule } from '@nestjs/sequelize';
import { PokemonEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon.entity';
import { PokemonTypeEntity } from '@/modules/pokemon/infraestructure/persistence/entity/pokemon-type.entity';

describe('PokemonApp Backend (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRoot({
          dialect: 'sqlite',
          storage: 'db/.cache/data.sqlite3',
          models: [PokemonEntity, PokemonTypeEntity],
        }),
        Modules,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  beforeEach(() => {
    migrateDBForTests();
  });

  afterAll(async () => {
    await app.close();
  });

  it('[GET] /pokemons', () => {
    return request(app.getHttpServer())
      .get('/pokemons')
      .expect(200)
      .expect([
        {
          id: 1,
          image:
            'https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png',
          name: 'Pikachu',
          expansion: 'Base Set',
          type: 'Electric',
          rarity: 'COMMON',
        },
      ]);
  });
});
