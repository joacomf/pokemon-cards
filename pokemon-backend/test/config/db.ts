import { spawnSync } from 'child_process';

export function migrateDBForTests() {
  spawnSync('rm', ['./db/.cache/data.sqlite3']);
  spawnSync('npx', ['sequelize-cli', 'db:migrate', '--env', 'local-test'], {
    stdio: 'inherit',
    cwd: './db',
  });
}
