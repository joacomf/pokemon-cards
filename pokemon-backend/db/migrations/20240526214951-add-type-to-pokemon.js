'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('pokemons', 'typeId', {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'pokemon_types',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    });
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('pokemons', 'typeId');
  },
};
