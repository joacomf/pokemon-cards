'use strict';

module.exports = {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  up: async (queryInterface, Sequelize) => {
    const pokemonTypes = [
      { name: 'Normal' },
      { name: 'Fire' },
      { name: 'Water' },
      { name: 'Electric' },
      { name: 'Grass' },
      { name: 'Ice' },
      { name: 'Fighting' },
      { name: 'Poison' },
      { name: 'Ground' },
      { name: 'Flying' },
      { name: 'Psychic' },
      { name: 'Bug' },
      { name: 'Rock' },
      { name: 'Ghost' },
      { name: 'Dragon' },
      { name: 'Dark' },
      { name: 'Steel' },
      { name: 'Fairy' },
    ];

    await queryInterface.bulkInsert('pokemon_types', pokemonTypes, {});
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('pokemon_types', null, {});
  },
};
