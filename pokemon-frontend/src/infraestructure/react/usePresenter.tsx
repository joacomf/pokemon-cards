import useInstance from '@use-it/instance'
import { useEffect, useReducer } from 'react'
import { Presenter } from '@/infraestructure/core/presenters/presenter';

export function usePresenter<TPresenter extends Presenter>(
  presenterFactory: (onChange: (model: any) => void) => TPresenter
): TPresenter {
    const forceUpdate = useReducer(() => ({}), {})[1] as () => void
    const presenter: TPresenter = useInstance(() => {
        const onModelChange = (_: any) => forceUpdate()
        return presenterFactory(onModelChange)
    });

    useEffect(() => {
        presenter.start?.()
        return () => {
            presenter.stop?.()
        }
    }, [presenter]);

    return presenter
}
