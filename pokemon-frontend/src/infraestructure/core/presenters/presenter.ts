export abstract class Presenter {
  abstract start(): void;
  stop(): void {};
}
