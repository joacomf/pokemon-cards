import { HomeScreenPresenter } from '@/ui/screens/home/HomeScreenPresenter';
import { AppConfig } from '@/infraestructure/core/app.config';
import { ChangeFunc } from '@/infraestructure/core/presenters/ChangeFunc';

export class PresenterFactory {
    constructor(private services: AppConfig) {}

    homeScreen = (onChange: ChangeFunc) => new HomeScreenPresenter(onChange, this.services.pokemon)
}
