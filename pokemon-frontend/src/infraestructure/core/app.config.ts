import { PokemonService } from '@/services/pokemon.service';

export interface AppConfig {
  pokemon: PokemonService,
}

export const defaultAppConfig = (): AppConfig => {
  return {
    pokemon: new PokemonService(),
  }
}
