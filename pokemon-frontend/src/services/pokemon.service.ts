import { Pokemon } from '@/domain/pokemon';
import axios from 'axios';

export class PokemonService {
  private readonly ALL_PATH = `/pokemons`;

  async getAll() {
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    };

    try {
      const url = `${process.env.NEXT_PUBLIC_API_URL}${this.ALL_PATH}`;
      const { data } = await axios.get<Pokemon[]>(url, { headers: headers });

      return data;

    } catch (e) {
      throw new Error('Unable to get pokemons from server');
    }
  }
}
