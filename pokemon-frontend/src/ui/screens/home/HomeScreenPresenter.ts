import { PokemonService } from '@/services/pokemon.service';
import { ChangeFunc } from '@/infraestructure/core/presenters/ChangeFunc';
import { Presenter } from '@/infraestructure/core/presenters/presenter';
import { PokemonsVM } from '@/ui/screens/home/PokemonsVM';

export class HomeScreenPresenter extends Presenter {
    model: PokemonsVM;

    constructor(private onChange: ChangeFunc, private pokemonService: PokemonService) {
        super();
        this.model = new PokemonsVM();
    }

    async start() {
        this.model.startLoading();
        this.onChange({...this.model});

        try {
            const pokemons = await this.pokemonService.getAll();
            this.model.update(pokemons);
        } catch (e: any) {
            this.model.error(e.message);
        }

        this.model.stopLoading();
        this.onChange({...this.model});
    }

}
