import React from 'react';
import { PokemonList } from '@/ui/components/pokemon/PokemonList';
import { usePresenterFactory } from '@/infraestructure/react/context/PresenterContext';
import { usePresenter } from '@/infraestructure/react/usePresenter';
import { Loader } from '@/ui/components/main/loader/loader';
import { ErrorAlert } from '@/ui/components/main/alerts/ErrorAlert';

export function HomeScreen() {
  const presenters = usePresenterFactory();
  const presenter = usePresenter(presenters.homeScreen)

  if (presenter.model.isLoading) return <Loader />
  if (presenter.model.errorMessage) return <ErrorAlert message={presenter.model.errorMessage} ></ErrorAlert>

  return (
    <div className="mx-auto max-w-2xl px-4 py-4 sm:px-6 sm:py-12 lg:max-w-7xl lg:px-8">
      <PokemonList pokemons={presenter.model.pokemons}/>
    </div>
  );
}
