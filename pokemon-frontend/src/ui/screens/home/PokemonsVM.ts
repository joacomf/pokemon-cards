import { Pokemon } from '@/domain/pokemon';

export class PokemonsVM {
  pokemons: Pokemon[] = [];
  isLoading = true;
  errorMessage?: string;

  startLoading() {
    this.isLoading = true;
  }

  stopLoading() {
    this.isLoading = false;
  }

  update(pokemons: Pokemon[]) {
    this.pokemons = pokemons;
  }

  error(message: string) {
    this.errorMessage = message;
  }
}
