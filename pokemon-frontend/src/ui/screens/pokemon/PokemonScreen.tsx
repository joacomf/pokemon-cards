import { Component } from 'react';

interface PokemonScreenProps {
  name?: string | string [];
}

export class PokemonScreen extends Component<PokemonScreenProps> {
  render() {
    return <>
      <div>{this.props.name}</div>
    </>
  }
}
