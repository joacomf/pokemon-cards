import { MainLayout } from '@/ui/layouts/MainLayout';
import { AppConfig } from '@/infraestructure/core/app.config';
import { PresenterFactory } from '@/infraestructure/core/presenters/presenter.factory';

export class PokemonApp {
  private readonly presenters: PresenterFactory;

  constructor(config: AppConfig) {
    this.presenters = new PresenterFactory(config);
  }

  render(PageComponent: any, pageProps = {}){
      return (
        <MainLayout presenters={this.presenters}>
          <PageComponent {...pageProps} />
        </MainLayout>
      )
    }
}
