import React, { Component } from 'react';
import { PokemonItem } from '@/ui/components/pokemon/PokemonItem';
import { Pokemon } from '@/domain/pokemon';

export type PokemonListProps = {
  pokemons: Pokemon[];
}

export class PokemonList extends Component<PokemonListProps> {
  render() {
    const pokemons = this.props.pokemons;

    return <div id={'pokemonList'} className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
      {pokemons.map((pokemon) => <PokemonItem key={pokemon.id} pokemon={pokemon}/>)}
    </div>;
  }
}
