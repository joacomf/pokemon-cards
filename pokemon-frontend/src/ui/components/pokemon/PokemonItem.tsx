import React, { Component } from 'react';
import { Pokemon } from '@/domain/pokemon';

export type PokemonItemProps = {
  pokemon: Pokemon
}

export class PokemonItem extends Component<PokemonItemProps> {
  render() {
    const pokemon = this.props.pokemon;

    return <a href={pokemon.href} className="group" id={"pokemon-" + pokemon.id} role="link">
      <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-lg bg-white p-5 xl:aspect-h-8 xl:aspect-w-7">
        <h3 className="mt-0 mb-2  text-lg font-medium text-gray-700" data-testid={'pokemon-name'}>{pokemon.name}</h3>
        <img
          src={pokemon.image}
          alt={pokemon.name}
          className="h-full w-full object-cover object-center group-hover:opacity-75"
          data-testid={'pokemon-image'}
        />
        <p className="mt-1 text-lg font-medium text-gray-700" data-testid={'pokemon-type'}>{pokemon.type}</p>
        <p className="mt-1 text-lg font-medium text-gray-700" data-testid={'pokemon-expansion'}>{pokemon.expansion}</p>
        <p className="mt-1 text-lg font-medium text-gray-700" data-testid={'pokemon-rarity'}>{pokemon.rarity}</p>
      </div>
    </a>;
  }
}
