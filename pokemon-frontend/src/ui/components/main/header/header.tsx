import React, { Component } from 'react';

export class Header extends Component {
  render() {
    return <>
      <div className="min-h-full">
        <nav className="bg-gray-800">
          <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
            <div className="flex h-16 items-center justify-between">
              <img className="h-10 w-24" src="/assets/images/pokemon-logo.svg" alt="Logo" />
            </div>
          </div>
        </nav>
      </div>
    </>;
  }
}
