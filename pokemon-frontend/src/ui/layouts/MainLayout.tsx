import React, { FC, ReactNode } from 'react'
import { Header } from '@/ui/components/main/header/header';
import { PresentersProvider } from '@/infraestructure/react/context/PresenterContext';
import { PresenterFactory } from '@/infraestructure/core/presenters/presenter.factory';

export const MainLayout: FC<Props> = (props) => (
  <PresentersProvider factory={props.presenters}>
    <Header/>
      <main>
        <div className="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
          {props.children}
        </div>
      </main>
  </PresentersProvider>
)

interface Props {
  presenters: PresenterFactory,
  children: ReactNode;
}
