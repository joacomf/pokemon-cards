import { AppProps } from 'next/app';
import { PokemonApp } from '@/ui/PokemonApp';
import '@/ui/layouts/styles/globals.css'
import { defaultAppConfig } from '@/infraestructure/core/app.config';

const application = new PokemonApp(defaultAppConfig());

export default ({ Component, pageProps }: AppProps) => application.render(Component, pageProps)

