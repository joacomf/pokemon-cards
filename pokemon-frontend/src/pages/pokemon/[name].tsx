import { PokemonScreen } from '@/ui/screens/pokemon/PokemonScreen';
import { useRouter } from 'next/router';


export default function PokemonPage() {
  const router = useRouter()
  const { name } = router.query;

  return <PokemonScreen name={name}></PokemonScreen>
}
