import { Rarity } from '@/domain/rarity';
import { PokemonType } from '@/domain/pokemonType';

export class Pokemon {
  id: string = '';
  href: string = '';
  image: string = '';
  name: string = '';
  rarity?: Rarity;
  type?: PokemonType;
  expansion: string = '';

  constructor(name: string) {
    this.name = name;
    this.href = `/pokemon/${this.name.toLowerCase()}`
  }
}


export class PokemonBuilder {
  private readonly pokemon: Pokemon;

  constructor(name: string) {
    this.pokemon = new Pokemon(name);
  }

  static create(name: string): PokemonBuilder {
    return new PokemonBuilder(name);
  }

  withId(id: string): PokemonBuilder {
    this.pokemon.id = id;
    return this;
  }

  withImage(image: string): PokemonBuilder {
    this.pokemon.image = image;
    return this;
  }

  withRarity(rarity: Rarity): PokemonBuilder {
    this.pokemon.rarity = rarity;
    return this;
  }

  withType(type: PokemonType): PokemonBuilder {
    this.pokemon.type = type;
    return this;
  }

  withExpansion(expansion: string): PokemonBuilder {
    this.pokemon.expansion = expansion;
    return this;
  }

  build(): Pokemon {
    return this.pokemon;
  }
}
