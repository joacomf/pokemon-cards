import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { Charmander, Squirtle } from '@test/fixtures/pokemons.fixture';
import { PokemonItem } from '@/ui/components/pokemon/PokemonItem';

describe('[Component] PokemonItem', () => {
  it('should render a pokemon with name and image', () => {
    render(<PokemonItem pokemon={Charmander} />);

    expect(screen.getByTestId('pokemon-name')).toHaveTextContent('Charmander');
    expect(screen.getByTestId('pokemon-image')).toHaveTextContent('');
  });

  it('should render a pokemon with rarity, expansion and type', () => {
    render(<PokemonItem pokemon={Squirtle} />);

    expect(screen.getByTestId('pokemon-type')).toHaveTextContent('WATER');
    expect(screen.getByTestId('pokemon-rarity')).toHaveTextContent('COMMON');
    expect(screen.getByTestId('pokemon-expansion')).toHaveTextContent('Base Set');
  });
});
