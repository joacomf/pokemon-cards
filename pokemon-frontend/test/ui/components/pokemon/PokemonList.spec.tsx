import '@testing-library/jest-dom'
import { render } from '@testing-library/react'
import { PokemonList } from '@/ui/components/pokemon/PokemonList';
import { Eevee, Jigglypuff, Pikachu } from '@test/fixtures/pokemons.fixture';

describe('[Component] PokemonList', () => {
  it('should list one item if only one available', () => {
    const pokemons = [Pikachu];

    let { container } = render(<PokemonList pokemons={pokemons} />);

    const pokemonsList = container.querySelector('#pokemonList');

    expect(pokemonsList?.children).toHaveLength(1);
  });

  it('should list multiple items if there is more than one', () => {
    const pokemons = [Pikachu, Jigglypuff, Eevee];

    let { container } = render(<PokemonList pokemons={pokemons} />);

    const pokemonsList = container.querySelector('#pokemonList');

    expect(pokemonsList?.children).toHaveLength(3);
  });
});
