import { instance, mock, when } from "ts-mockito";
import { PokemonService } from '@/services/pokemon.service';
import { HomeScreenPresenter } from '@/ui/screens/home/HomeScreenPresenter';
import { ChangeFunc } from '@/infraestructure/core/presenters/ChangeFunc';
import { Eevee, Pikachu } from '@test/fixtures/pokemons.fixture';

describe('[Screen Presenter] HomeScreenPresenter', () => {

    it('shows and hides a loader on start called', async () => {
        await presenter.start();

        expect(changeMock).toHaveBeenCalledWith(expect.objectContaining({ isLoading: true }))
        expect(changeMock).toHaveBeenCalledWith(expect.objectContaining({ isLoading: false }))

        expect(presenter.model.isLoading).toBe(false);
    });

    it('should update pokemons in the model to show', async () => {
        const pokemons = [Pikachu, Eevee];
        when(service.getAll()).thenResolve(pokemons);

        await presenter.start();

        expect(presenter.model.pokemons).toEqual(pokemons)
    });


    it('shows update errorMessage when an error is throw', async () => {
        const errorMessage: string = 'Unable to get pokemons from the server';
        when(service.getAll()).thenReject(new Error(errorMessage))

        await presenter.start();

        expect(presenter.model.errorMessage).toEqual(errorMessage)
    })

    beforeEach(() => {
        service = mock<PokemonService>(PokemonService);
        changeMock = jest.fn();
        presenter = new HomeScreenPresenter(changeMock, instance(service))
    })

    let service: PokemonService;
    let presenter: HomeScreenPresenter;
    let changeMock: ChangeFunc;
});
