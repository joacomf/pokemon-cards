import { PokemonBuilder } from '@/domain/pokemon';
import { Rarity } from '@/domain/rarity';
import { PokemonType } from '@/domain/pokemonType';

export const Pikachu = PokemonBuilder.create("Pikachu")
  .withId("25")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png")
  .withRarity(Rarity.COMMON)
  .withType(PokemonType.ELECTRIC)
  .withExpansion("Base Set")
  .build();

export const Charmander = PokemonBuilder.create("Charmander")
  .withId("4")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png")
  .withRarity(Rarity.COMMON)
  .withType(PokemonType.FIRE)
  .withExpansion("Base Set")
  .build();

export const Squirtle = PokemonBuilder.create("Squirtle")
  .withId("7")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png")
  .withRarity(Rarity.COMMON)
  .withType(PokemonType.WATER)
  .withExpansion("Base Set")
  .build();

export const Bulbasaur = PokemonBuilder.create("Bulbasaur")
  .withId("1")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png")
  .withRarity(Rarity.COMMON)
  .withType(PokemonType.GRASS)
  .withExpansion("Base Set")
  .build();

export const Jigglypuff = PokemonBuilder.create("Jigglypuff")
  .withId("39")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/039.png")
  .withRarity(Rarity.UNCOMMON)
  .withType(PokemonType.FAIRY)
  .withExpansion("Base Set")
  .build();

export const Eevee = PokemonBuilder.create("Eevee")
  .withId("133")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/133.png")
  .withRarity(Rarity.UNCOMMON)
  .withType(PokemonType.NORMAL)
  .withExpansion("Jungle")
  .build();

export const Snorlax = PokemonBuilder.create("Snorlax")
  .withId("143")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/143.png")
  .withRarity(Rarity.RARE)
  .withType(PokemonType.NORMAL)
  .withExpansion("Jungle")
  .build();

export const Gengar = PokemonBuilder.create("Gengar")
  .withId("94")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/094.png")
  .withRarity(Rarity.RARE)
  .withType(PokemonType.GHOST)
  .withExpansion("Fossil")
  .build();

export const Gyarados = PokemonBuilder.create("Gyarados")
  .withId("130")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/130.png")
  .withRarity(Rarity.RARE)
  .withType(PokemonType.WATER)
  .withExpansion("Base Set")
  .build();

export const Mewtwo = PokemonBuilder.create("Mewtwo")
  .withId("150")
  .withImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/150.png")
  .withRarity(Rarity.RARE)
  .withType(PokemonType.PSYCHIC)
  .withExpansion("Base Set")
  .build();

export const PokemonList = [
  Pikachu,
  Charmander,
  Squirtle,
  Bulbasaur,
  Jigglypuff,
  Eevee,
  Snorlax,
  Gengar,
  Gyarados,
  Mewtwo,
]
