import nock from 'nock';
import { PokemonList } from '@test/fixtures/pokemons.fixture';
import { PokemonService } from '@/services/pokemon.service';

process.env.API_URL = 'http://localhost'

describe('[Service] PokemonService', () => {
  let service: PokemonService;

  beforeEach(() => {
    nock.cleanAll();
    service = new PokemonService();
  })

  it('should return an array with pokemons', async () => {
    givenThereArePokemonsAvailable();

    let pokemons = await service.getAll();

    expect(pokemons).toEqual(PokemonList);
  })

  it('should throw an error if is unable to fetch info', async () => {
    givenThereIsAnErrorInFetching();

    await expect(service.getAll()).rejects.toThrowError(new Error('Unable to get pokemons from server'));
  })

  function givenThereArePokemonsAvailable() {
    nock('http://localhost')
      .get('/pokemons')
      .reply(200, PokemonList)
  }

  function givenThereIsAnErrorInFetching() {
    nock('http://localhost').options(/.*/).reply(200)
    nock('http://localhost')
      .get('/pokemons')
      .reply(500)
  }

});
