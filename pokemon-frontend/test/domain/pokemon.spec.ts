import { PokemonBuilder } from '@/domain/pokemon';
import { Rarity } from '@/domain/rarity';
import { PokemonType } from '@/domain/pokemonType';

describe('PokemonBuilder', () => {
  it('should create a Pikachu with the correct properties', () => {
    const pikachu = PokemonBuilder.create('Pikachu')
      .withId('25')
      .withImage('https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png')
      .withRarity(Rarity.COMMON)
      .withType(PokemonType.ELECTRIC)
      .withExpansion('Base Set')
      .build();

    expect(pikachu.name).toBe('Pikachu');
    expect(pikachu.id).toBe('25');
    expect(pikachu.image).toBe('https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png');
    expect(pikachu.rarity).toBe(Rarity.COMMON);
    expect(pikachu.type).toBe(PokemonType.ELECTRIC);
    expect(pikachu.expansion).toBe('Base Set');
    expect(pikachu.href).toBe('/pokemon/pikachu');
  });

  it('should generate correct href for different Pokémon names', () => {
    const bulbasaur = PokemonBuilder.create('Bulbasaur')
      .withId('1')
      .withImage('https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png')
      .withRarity(Rarity.COMMON)
      .withType(PokemonType.GRASS)
      .withExpansion('Base Set')
      .build();

    expect(bulbasaur.href).toBe('/pokemon/bulbasaur');

    const squirtle = PokemonBuilder.create('Squirtle')
      .withId('7')
      .withImage('https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png')
      .withRarity(Rarity.COMMON)
      .withType(PokemonType.WATER)
      .withExpansion('Base Set')
      .build();

    expect(squirtle.href).toBe('/pokemon/squirtle');
  });
});
